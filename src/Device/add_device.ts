import { Component } from "@angular/core";
import { AppDeviceService } from "./add_device.service";
import { Router } from "@angular/router";

@Component({

    selector: 'addDevice-page',
    templateUrl: 'add_device.html',
    styleUrls: ['add_device.css'],
    providers: [AppDeviceService]
})

export class AppDevice {



    ngOnInit() {
        this.url = localStorage.getItem('urlAuthentication');
        console.log(this.url)
        if (this.url == "true") {
            this.user_details();
        }
        else  {
            alert("Please Log in!!")
            this.router.navigate(['/Login'])
        }
    }
    url;
    details = [];
    device_detail = [];
    message = [];
    visible: boolean = true;
    constructor(private router: Router, private service: AppDeviceService) {
        this.url = localStorage.getItem('urlAuthentication');
        console.log(this.url)
    }

    user_details() {
        // alert();
        this.service.getDevice().subscribe(
            data => {
                this.details = data;
                console.log(this.details);
                if (this.details.length == 0 || this.details == undefined) {
                    this.visible = false
                }
                else {
                    this.visible = true;
                }

            },
            err => console.log(err)
        )
    }

    add_user() {
        this.service.getNewDevice().subscribe(
            data => {
                this.device_detail = data;
                console.log(this.device_detail[0].device_id)
                var device = { device_id: this.device_detail[0].device_id };
                this.details.push(device);
                this.user_details();
            },
            err => console.log(err)
        )
    }

    // remove(index) {


    //     // this.service.deleteDevice(this.details[index]).subscribe(
    //     //     data => {
    //     //         this.message = data[0].message;
    //     //         alert(this.message);
    //     //         this.user_details();
    //     //     },
    //     //     err => console.log(err)
    //     // )
    // }
}