import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()

export class AppDeviceService {
    private headers = new Headers({ 'content-type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    ip = "http://103.14.120.213:3001";

    getDevice(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getalldevices").map(res => res.json());
    }
    getNewDevice(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getnewdeviceid").map(res => res.json());
    }
    // deleteDevice(device_id): Observable<any> {

    //     return this.http.get(this.ip + "/api/adminpage/deletedevice?device_id=" + device_id).map(res => res.json());
    // }
}