import { Component } from "@angular/core";
import { AppNavigationService } from "./navigation.service";
import { Router } from "@angular/router";

@Component({

    selector: 'navigation-bar',
    templateUrl: 'navigation.html',
    styleUrls: ['navigation.css'],
    providers: [AppNavigationService]
})

export class AppNavigation {

    logout;
    url;
    constructor(private service: AppNavigationService, private router: Router) {

        this.url = localStorage.getItem('urlAuthentication');

    }

    logOut() {


        this.url = localStorage.setItem("urlAuthentication", "false");
        this.router.navigate(['/Login']);
        // this.service.send_logOut().subscribe(
        //     data => {
        //         this.logout = data;
        //         if (this.logout[0].message == "logout successfull") {
        //             this.router.navigate(['/Login']);
        //         }
        //     },
        //     err => console.log(err)
        // )
    }
}