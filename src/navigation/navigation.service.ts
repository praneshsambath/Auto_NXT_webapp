import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()

export class AppNavigationService {
    private headers = new Headers({ 'content-type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    ip = "http://103.14.120.213:3001";

    // send_logOut(): Observable<any> {
    //     return this.http.get(this.ip + "/api/adminpage/adminlogout").map(res => res.json());
    // }
}