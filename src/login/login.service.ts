import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()

export class AppLoginService {
    private headers = new Headers({ 'content-type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    ip = "http://103.14.120.213:3001";

    getUsernamePassword(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getadmindetail").map(res => res.json());
    }

    // checkUsernamePassword(username, password): Observable<any> {
    //     return this.http.get(this.ip + "/api/adminpage/adminlogin?username=" + username + "&password=" + password).map(res => res.json());
    // }

}