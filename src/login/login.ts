import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppLoginService } from "./login.service";

@Component({

    selector: "Login-page",
    templateUrl: 'login.html',
    styleUrls: ['login.css'],
    providers: [AppLoginService]
})

export class AppLogin {

    login = { username: '', password: '' };
    username_password;
    status: boolean; empty: boolean;
    isLogin; multiple_login: boolean;
    url;
    ngOnInit() {
        this.getLoginDetails();
    }
    constructor(private router: Router, private service: AppLoginService) {
        this.url = localStorage.getItem('urlAuthentication', )
    }
    getLoginDetails() {
        this.service.getUsernamePassword().subscribe(
            data => {
                this.username_password = data

            },
            err => console.log(err)
        )
    }
    OnSubmit(login) {
        if ((login.username.length != 0) && (login.password.length != 0)) {
            for (let details of this.username_password) {

                if ((login.username == details.admin_username) && (login.password == details.admin_password)) {
                    // this.service.checkUsernamePassword(login.username, login.password).subscribe(
                    //     data => {
                    //         this.isLogin = data;
                    //         console.log(data);
                    //         if( this.isLogin[0].message == "Admin login successfull")
                    //         {
                    //             this.router.navigate(['/User']);
                    //         }
                    //         else
                    //         {
                    //            this.multiple_login = true;
                    //            this.status = false;
                    //         }
                    //     },
                    //     err => console.log(err)
                    // )
                    localStorage.setItem("urlAuthentication", "true");
                    this.router.navigate(['/User']);
                }
                else {
                    this.status = true;
                    this.empty = false;
                }
            }
        }
        else {
            this.empty = true;
            this.status = false;
        }
    }
}