import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AppLogin } from '../login/login';
import { FormsModule } from '@angular/forms';
import { AppUser, SearchFilter } from '../user/user';
import { AppNavigation } from '../navigation/navigation';
import { AppDevice } from '../Device/add_device';
import { AppReport } from '../Report/report';
import { AppMapping } from '../Mapping/mapping';
import { AppUserService } from '../user/user.service';
import { HttpModule } from '@angular/http';
import { AppNoPage } from '../No_page/pageNotFound';
import { Ng2PaginationModule } from 'ng2-pagination';
// import { DataTablesModule } from 'angular-datatables';


export const routes: Routes = [
  { path: 'Home', component: AppComponent },
  { path: 'Login', component: AppLogin },
  { path: 'User', component: AppUser },
  // { path: 'Navbar', component: AppNavigation },
  { path: 'Device', component: AppDevice },
  { path: 'Mapping', component: AppMapping },
  { path: 'Report', component: AppReport },
  { path: '**', component: AppNoPage }
]


@NgModule({
  declarations: [
    AppComponent, AppLogin, AppUser, AppNavigation, AppDevice, AppMapping, AppReport, AppNoPage,SearchFilter
  ],
  imports: [
    BrowserModule, FormsModule, RouterModule.forRoot(routes), HttpModule, Ng2PaginationModule
  ],
  providers: [AppUserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
