import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from '@angular/http';

@Component({

    selector: "nopage-page",
    templateUrl: 'pageNotFound.html',

})

export class AppNoPage {

}