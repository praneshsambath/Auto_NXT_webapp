import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppMappingService } from "./mapping.service";
import { CssSelector } from "@angular/compiler";


import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchFilter'
})
export class SearchFilter implements PipeTransform {
    transform(items: any[], criteria: any): any {

        return items.filter(item => {
            for (let key in item) {
                if (("" + item[key]).includes(criteria)) {
                    return true;
                }
            }
            return false;
        });
    }
}

@Component({

    selector: 'mapping-page',
    templateUrl: 'mapping.html',
    styleUrls: ['mapping.css'],
    providers: [AppMappingService]
})

export class AppMapping {

    url;
    details = []; id;
    visible: boolean = true;
    new_device_id; new_customer_id;

    ngOnInit() {
        if (this.url == "true") {
            this.user_details();
        }
        else {
            alert("Please Log in!!")
            this.router.navigate(['/Login'])
        }
    }

    constructor(private router: Router, private service: AppMappingService) {

        this.url = localStorage.getItem('urlAuthentication');
    }

    user_details() {

        this.service.getMapping().subscribe(
            data => {
                this.details = data
                if (this.details.length == 0 || this.details == undefined) {
                    this.visible = false
                }
                else {
                    this.visible = true;
                }
            },
            err => console.log(err)
        )
    }

    device_ID() {
        this.service.get_deviceId().subscribe(
            data => {
                this.new_device_id = data
            },
            err => console.log(err)
        )
        this.service.get_customerId().subscribe(
            data => {
                this.new_customer_id = data
            },
            err => console.log(err)
        )

    }
    map_device(dev_id, cust_id) {
        this.service.send_devId_cusId(dev_id, cust_id).subscribe(
            data => {
                var result = data
                this.user_details();
            },
            err => console.log(err)
        )
    }
    OnClickremove(det) {
        this.id = { device_id: det.device_id, customer_id: det.cus_id };

    }
    remove() {

        this.service.delete_mapped_device(this.id.device_id, this.id.customer_id).subscribe(
            data => {
                var result = data
                this.user_details();
            },
            err => console.log(err)
        )
    }
}