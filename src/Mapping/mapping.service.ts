import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()

export class AppMappingService {
    private headers = new Headers({ 'content-type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    ip = "http://103.14.120.213:3001";

    getMapping(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getallmapping").map(res => res.json());
    }
    get_deviceId(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getaddeddevices").map(res => res.json());
    }
    get_customerId(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getallcustomers").map(res => res.json());
    }
    send_devId_cusId(device_id, customer_id): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/savemappingnewdata?device_id=" + device_id + "&cus_id=" + customer_id).map(res => res.json());
    }
    delete_mapped_device(dev_id, cus_id): Observable<any> {
        console.log(dev_id,cus_id)
        return this.http.get(this.ip + "/api/adminpage/deletemapping?device_id=" + dev_id + "&cus_id=" + cus_id).map(res => res.json());
    }
}