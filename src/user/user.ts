import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppUserService } from "./user.service";
import { Http } from '@angular/http';
import { PaginatePipe, PaginationService } from 'ng2-pagination';

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchFilter'
})
export class SearchFilter implements PipeTransform {
    transform(items: any[], criteria: any): any {

        return items.filter(item => {
            for (let key in item) {
                if (("" + item[key]).includes(criteria)) {
                    return true;
                }
            }
            return false;
        });
    }
}


@Component({

    selector: "User-page",
    templateUrl: 'user.html',
    styleUrls: ['user.css'],
    providers: [AppUserService]
})

export class AppUser {

    details = [];
    visible: boolean = true;
    edit_details = {}; remove_details = {};
    remove_status: boolean;
    delete_cus_id: number;
    add_customer = {};
    add_customer_id;
    user_status: boolean; error;
    url;
    ngOnInit() {
        if (this.url == "true") {
            this.user_details();
        }
        else {
            alert("Please Log in!!")
            this.router.navigate(['/Login'])
        }
    }

    constructor(private router: Router, private service: AppUserService) {

        this.url = localStorage.getItem('urlAuthentication');
        // console.log(url);
    }


    user_details() {
        this.service.getUser().subscribe(
            data => {
                this.details = data;
                if (this.details.length == 0 || this.details == undefined) {
                    this.visible = false
                }
                else {
                    this.visible = true;
                }

            },
            err => console.log(err))
    }

    edit(selected_details) {
        this.edit_details = { customer_id: selected_details.cus_id, mobile_number: selected_details.cus_mobile, customer_name: selected_details.cus_name };
    }
    save(edited_details) {
        this.service.Edit_and_save(edited_details.customer_id, edited_details.customer_name, edited_details.mobile_number).subscribe(
            data => {
                var after_edit = data;
                alert("Saved Successfully!!!")
                this.user_details();
            },
            err => console.log(err)
        )

    }
    Onclick_remove(det) {
        this.remove_status = true;
        this.delete_cus_id = det.cus_id;

    }
    delete() {
        this.service.delete_customer_details(this.delete_cus_id).subscribe(

            data => {
                var after_deleted = data;
                alert("Deleted Successfully!!!");
                this.user_details();
            },
            err => console.log(err)
        )
    }
    Adduser() {
        this.service.get_customer_id().subscribe(

            data => {
                this.add_customer_id = data[0].cus_id;
                this.add_customer = { cus_id: this.add_customer_id, cus_mobile: "", cus_name: "" }
            },
            err => console.log(err)
        )
    }
    Add(details) {

        this.service.Add_details(details.cus_id, details.cus_name, details.cus_mobile).subscribe(

            data => {
                var add = data;
                if (add[0].message == "New user added successfully") {
                    this.add_customer = { cus_id: this.add_customer_id, cus_mobile: details.mobile_number, cus_name: details.customer_name };
                    this.details.push(this.add_customer);
                    this.user_details();
                }
                else {
                    alert();
                    this.user_status = true;
                }
            },
            err => console.log(err)
        )
    }
    onChange(value) {
        console.log(value.toString().length)
        // if(value.toString().length == 10)
        // {
        //     this.user_status = true;
        // }
    }
}



