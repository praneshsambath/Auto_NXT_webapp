import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()

export class AppUserService {
    private headers = new Headers({ 'content-type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    ip = "http://103.14.120.213:3001";

    getUser(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getallcustomerdetail").map(res => res.json());
    }
    Edit_and_save(cus_id, cus_name, cus_mobile): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/updateadduserdata?cus_id=" + cus_id + "&cus_name=" + cus_name + "&cus_mobile=" + cus_mobile).map(res => res.json());
    }
    delete_customer_details(cus_id): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/deleteuser?cus_id=" + cus_id).map(res => res.json());
    }
    get_customer_id(): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/getnewcustomerid").map(res => res.json());
    }

    Add_details(cus_id, cus_name, cus_mobile): Observable<any> {
        return this.http.get(this.ip + "/api/adminpage/adduser?cus_id=" + cus_id + "&cus_name=" + cus_name + "&cus_mobile=" + cus_mobile).map(res => res.json());
    }
}